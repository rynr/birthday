'use strict';

const calculations = {
  'SECONDS' : [100000000, 123456789, 500000000, 666666666, 1000000000,
    1111111111, 1500000000, 1234567890, 2000000000, 2222222222, 2500000000,
    3000000000, 3333333333, 4000000000],
  'MINUTES' : [100000, 1000000, 5000000, 10000000, 11111111, 12345678,
    15000000, 20000000, 22222222, 25000000, 30000000, 33333333, 35000000,
    40000000, 42000000, 44444444, 45000000, 50000000, 55555555],
  'HOURS'   : [50000, 100000, 111111, 123456, 150000, 200000, 222222, 300000,
    333333, 350000, 400000, 444444, 450000, 500000, 550000, 555555, 600000,
    650000, 666666, 700000, 750000, 777777, 800000, 850000, 888888, 900000,
    950000],
  'DAYS'    : [100, 123, 250, 333, 400, 500, 666, 750, 1000, 1111, 1234, 1500,
    2500, 5000, 5555, 6666, 7000, 7500, 8888, 10000, 11111, 12345, 15000,
    20000, 22222, 25000, 30000, 33333, 35000, 40000, 44444, 45000, 50000],
  'WEEKS'   : [42, 100, 123, 250, 333, 400, 500, 666, 750, 1000, 1111, 1234,
    1500, 2000, 2222, 2500, 3000, 3333, 4000, 4444, 5000, 5555, 6000, 6666],
  'MONTHS'  : [42, 100, 111, 123, 150, 250, 333, 400, 444, 500, 555, 600, 666,
    750, 777, 888, 999, 1000, 1111, 1234, 1250, 1500],
  'YEARS'   : [10, 11, 25, 33, 42, 50, 66, 75, 100, 111]
};
const multipliers = {
  'SECONDS' : 1000,
  'MINUTES' : 60000,
  'HOURS'   : 3600000,
  'DAYS'    : 86400000,
  'WEEKS'   : 604800000
};

class FreakingBirthday {

  static calculations(period) {
    return calculations[period];
  }

  static get periods() {
    return Object.keys(calculations);
  }

  constructor(birthday) {
    this.birthday = birthday;
  }

  birthdays() {
    return FreakingBirthday.periods.map(perios =>
      FreakingBirthday.calculations(perios).map(count =>
        {
          return {
            "count": count,
            "period": perios,
            "at": this.calculate(count, perios)
          }
        }
      )
    ).reduce((acc, val) => acc.concat(val), []).sort((a, b) => {
      return a.at - b.at;
    });
  }

  calculate(count, period) {
    switch(period) {
      case 'MONTHS':
        return this._calculateMonth(count, period);
      case 'YEARS':
        return this._calculateYear(count, period);
      default:
        return new Date(this.birthday.getTime() + multipliers[period] * count);
    }
  }

  _calculateYear(period) {
    var result = new Date(this.birthday);
    result.setFullYear(result.getFullYear() + period);
    return result;
  }

  _calculateMonth(period) {
    var result = new Date(this.birthday);
    result.setMonth(result.getMonth() + period);
    return result;
  }
}

module.exports = FreakingBirthday;
