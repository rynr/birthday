'use strict'

const FreakingBirthday = require('./freaking-birthday');
const expect = require('chai').expect;

describe('FreakingBirthday module', () => {
  describe('periods', () => {
    it('should export a function', () => {
      expect(FreakingBirthday.periods).to.be.eql(['SECONDS', 'MINUTES', 'HOURS',
        'DAYS', 'WEEKS', 'MONTHS', 'YEARS']);
    });
  });
  describe('calculations()', () => {
    FreakingBirthday.periods.forEach(function(period) {
      it('should provide counts for ' + period, () => {
        expect(FreakingBirthday.calculations(period)).to.be.a('Array');
      });
    });
    it('should not provide counts for unknown periods', () => {
      expect(FreakingBirthday.calculations('123')).to.be.undefined;
      expect(FreakingBirthday.calculations('abc')).to.be.undefined;
    });
    it('should not provide counts for undefined periods', () => {
      expect(FreakingBirthday.calculations()).to.be.undefined;
    });
  });
  describe('calculate()', () => {
    describe('calculate("SECONDS")', () => {
      it('same second', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'SECONDS')).to.be.eql(new Date(1975, 0));
      });
      it('one second', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'SECONDS')).to.be.eql(new Date(1975, 0, 1, 0, 0, 1));
      });
    });
    describe('calculate("MINUTES")', () => {
      it('same minute', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'MINUTES')).to.be.eql(new Date(1975, 0));
      });
      it('one minute', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'MINUTES')).to.be.eql(new Date(1975, 0, 1, 0, 1));
      });
    });
    describe('calculate("HOURS")', () => {
      it('same hour', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'HOURS')).to.be.eql(new Date(1975, 0));
      });
      it('one hour', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'HOURS')).to.be.eql(new Date(1975, 0, 1, 1));
      });
    });
    describe('calculate("DAYS")', () => {
      it('same day', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'DAYS')).to.be.eql(new Date(1975, 0));
      });
      it('one day', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'DAYS')).to.be.eql(new Date(1975, 0, 2));
      });
    });
    describe('calculate("WEEKS")', () => {
      it('same week', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'WEEKS')).to.be.eql(new Date(1975, 0));
      });
      it('one week', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'WEEKS')).to.be.eql(new Date(1975, 0, 8));
      });
    });
    describe('calculate("MONTHS")', () => {
      it('same month', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'MONTHS')).to.be.eql(new Date(1975, 0));
      });
      it('one month', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'MONTHS')).to.be.eql(new Date(1975, 1));
      });
      it('12 months', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(12, 'MONTHS')).to.be.eql(new Date(1976, 0));
      });
      it('14 months', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(14, 'MONTHS')).to.be.eql(new Date(1976, 2));
      });
      it('1 months for a 31th', () => {
        expect(new FreakingBirthday(new Date(1975, 0, 31)).calculate(1, 'MONTHS')).to.be.eql(new Date(1975, 2, 3));
      });
      it('1 months for a 31th (leap year)', () => {
        expect(new FreakingBirthday(new Date(1976, 0, 31)).calculate(1, 'MONTHS')).to.be.eql(new Date(1976, 2, 2));
      });
    });
    describe('calculate("YEARS")', () => {
      it('same year', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(0, 'YEARS')).to.be.eql(new Date(1975, 0));
      });
      it('one year', () => {
        expect(new FreakingBirthday(new Date(1975, 0)).calculate(1, 'YEARS')).to.be.eql(new Date(1976, 0));
      });
      it('10 years', () => {
        expect(new FreakingBirthday(new Date(1990, 0)).calculate(10, 'YEARS')).to.be.eql(new Date(2000, 0));
      });
    });
  });
  describe('birthdays', () => {
    it('should export a function', () => {
      expect(new FreakingBirthday(new Date(1975, 0)).birthdays()).to.be.a('Array');
    });
  });
});
