freaking-birthday
=================

Installation
------------

Using npm:

```bash
$ npm i --save freaking-birthday
```

Usage
-----

```bash
var FreakingBirthday = require('freaking-birthday');
console.log(new FreakingBirthday(new Date(1999, 0)).birthdays());
```

More to come, the library is not yet done, feel free to propose a better
structure, as I'm not that firm with best practices for JavaScript.

Links
-----

[![Try freaking-birthday on RunKit](https://badge.runkitcdn.com/freaking-birthday.svg)](https://npm.runkit.com/freaking-birthday)
[![pipeline status](https://gitlab.com/rynr/birthday/badges/library/pipeline.svg)](https://gitlab.com/rynr/birthday/commits/library)
[[bundlephobia](https://bundlephobia.com/result?p=freaking-birthday)]
